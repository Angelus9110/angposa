#Práctica 4 TDS
##Ángel Posada García

Aunque esta práctica contiene un proyecto en Eclipse se recomienda usa unicamente ANT con los objetivos implementados.

En primer lugar se ha de situar en la carpeta del proyecto

`cd angposa`

A continuación mostramos la lista de los posibles comandos implementados para ANT

| Comando | Descripción |
| --- | --- |
| `ant limpiar` | Limpia el directorio Target |
| `ant compilar` | Compila todos los ficheros fuente, incluidos los test |
| `ant ejecutarTodo` | Ejecuta todos los test del proyecto |
| `ant ejecutarTestTDD` | Ejecuta todos los test de caja negra |
| `ant ejecutarTestEnAislamiento` | Ejecuta todos los tests de aislamiento |
| `ant ejecutarPruebasSecuencia` | Ejecuta todas las pruebas de secuencia |
| `ant obtenerInformeCobertura` | Crea informe de cobertura. Se podrá consultar en el fichero HTML `tarjet/site/emma/index.html` |
| `ant medidas` | Crea informe de medidas. Se podrá consultar en el fichero HTML `tarjet/site/cobertura/index.html` |
| `ant documentar` | Crea la documentación de las clases a partir del javadoc. Se podrá consultar en el fichero HTML `tarjet/site/apidocs/index.html` |

Se puede observar que todos los objetivos ANT descansan en llamadas a Maven.
