package es.uva.inf.tds.entornoeducativo.angposa;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class TestAsignaturaTDD {
	private Date fechaInicio;
	private Date fechaFin;
	private Asignatura asignaturaOriginal;
	private double errorAdmisible = 0.001;
	
	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		fechaInicio = new Date(2015, 0, 1);
		fechaFin = new Date(2015, 3, 1);
		asignaturaOriginal = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
	}

	@Test
	public void testCrearAsignatura() {
		Asignatura asignatura = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		assertEquals("nombre", asignatura.getNombre());
		assertEquals("descripcion", asignatura.getDescripcion());
		assertEquals(10.0, asignatura.getNotaMaxima(), errorAdmisible);
		assertEquals(fechaInicio, asignatura.getFechaInicio());
		assertEquals(fechaFin, asignatura.getFechaFin());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoticiaConNotaMaximaFueraRangoArriba() {
		@SuppressWarnings("unused")
		Asignatura asignatura = new Asignatura("nombre", "descripcion", 11.0, fechaInicio, fechaFin);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoticiaConNotaMaximaFueraRangoAbajo() {
		@SuppressWarnings("unused")
		Asignatura asignatura = new Asignatura("nombre", "descripcion", 0.0, fechaInicio, fechaFin);
	}

	@Test
	public void testNotaErrorAdmisible() {
		Asignatura asignatura = new Asignatura("nombre", "descripcion", 7.0, fechaInicio, fechaFin);
		assertNotNull(asignatura);
		assertEquals(asignatura.getNotaMaxima(), 7.0, errorAdmisible);
	}

	@Test
	public void testGetNombre() {
		String s = asignaturaOriginal.getNombre();
		assertEquals("nombre", s);
	}

	@Test
	public void testGetDescripcion() {
		String s = asignaturaOriginal.getDescripcion();
		assertEquals("descripcion", s);
	}

	@Test
	public void testGetNotaMaxima() {
		double d = asignaturaOriginal.getNotaMaxima();
		assertTrue(10.0 == d);
	}

	@Test
	public void testGetFechaInicio() {
		Date c = asignaturaOriginal.getFechaInicio();
		assertEquals(fechaInicio, c);
	}

	@Test
	public void testGetFechaFin() {
		Date c = asignaturaOriginal.getFechaFin();
		assertEquals(fechaFin, c);
	}

	
}