package es.uva.inf.tds.entornoeducativo.angposa;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Hashtable;

import org.junit.Before;
import org.junit.Test;

public class TestSecuencia {
	private Date fechaInicio;
	private Date fechaFin;
	private Date fechaCalificacion;
	private Asignatura asignaturaOriginal;
	private Prueba prueba1;
	private Prueba prueba2;
	private Prueba prueba3;
	private Date fechaRealizacion;
	private double errorAdmisible = 0.01;

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		fechaInicio = new Date(2015, 0, 1);
		fechaFin = new Date(2015, 3, 1);
		fechaCalificacion = new Date(2015, 2, 15);
		fechaRealizacion = new Date(2015, 2, 5);
		asignaturaOriginal = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		prueba1 = new Prueba(fechaRealizacion, "nombre", "descripcion", 10, 0.5);
		prueba2 = new Prueba(fechaRealizacion, "nombre", "descripcion", 10, 0.3);
		prueba3 = new Prueba(fechaRealizacion, "nombre", "descripcion", 10, 0.2);
	}

	@Test
	public void testSecuenciaCreacion() {
		assertNotNull(asignaturaOriginal);
		assertEquals("nombre", asignaturaOriginal.getNombre());
		assertEquals("descripcion", asignaturaOriginal.getDescripcion());
		assertEquals(10.0, asignaturaOriginal.getNotaMaxima(), errorAdmisible);
		assertEquals(fechaInicio, asignaturaOriginal.getFechaInicio());
		assertEquals(fechaFin, asignaturaOriginal.getFechaFin());
		
		
		asignaturaOriginal.incluirPrueba(prueba1);
		asignaturaOriginal.incluirPrueba(prueba2);
		assertTrue(asignaturaOriginal.numeroPruebas() == 2);
		assertTrue(asignaturaOriginal.pruebasCompletamenteCalificadas() == false);
		
		
		prueba1.calificar("2a1", 4.0, fechaCalificacion);
		prueba1.marcarCalificada(fechaCalificacion);
		assertTrue(asignaturaOriginal.pruebasCompletamenteCalificadas() == false);
		
		
		prueba2.calificar("2a1", 8.0, fechaCalificacion);
		prueba2.marcarCalificada(fechaCalificacion);
		assertTrue(asignaturaOriginal.pruebasCompletamenteCalificadas() == true);
		

		assertTrue(asignaturaOriginal.contienePrueba(prueba1));
		assertTrue(asignaturaOriginal.contienePrueba(prueba2));
		assertFalse(asignaturaOriginal.contienePrueba(prueba3));
		
		
		Hashtable<String, Double> calificacionesParciales = new Hashtable<String, Double>();
		calificacionesParciales = asignaturaOriginal.calificacionesParciales();
		assertNotNull(calificacionesParciales);
		assertTrue(asignaturaOriginal.pesoPruebas() <= 1);
		
		
		asignaturaOriginal.incluirPrueba(prueba3);
		prueba3.calificar("2a1", 8.0, fechaCalificacion);
		prueba3.marcarCalificada(fechaCalificacion);
		assertTrue(asignaturaOriginal.contienePrueba(prueba3));
		assertTrue(asignaturaOriginal.pruebasCompletamenteCalificadas() == true);
		
		Hashtable<String, Double> calificacionesFinales = new Hashtable<String, Double>();
		calificacionesFinales = asignaturaOriginal.calificacionesFinales();
		assertNotNull(calificacionesFinales);
		assertTrue(asignaturaOriginal.pesoPruebas() == 1.0);
	}
}