package es.uva.inf.tds.entornoeducativo.angposa;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestAsignaturaTDD.class, TestSecuencia.class, TestAsignaturaTDD2.class })

public class TestBlackBox {

}