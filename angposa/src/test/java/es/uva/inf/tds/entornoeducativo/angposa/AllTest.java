package es.uva.inf.tds.entornoeducativo.angposa;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestAsignaturaTDD.class, TestAsignaturaTDD2.class, TestSecuencia.class, MockingTest.class })

public class AllTest {

}