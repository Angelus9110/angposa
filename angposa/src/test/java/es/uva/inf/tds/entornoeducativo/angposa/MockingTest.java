package es.uva.inf.tds.entornoeducativo.angposa;

import java.util.Date;
import java.util.Hashtable;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import org.easymock.EasyMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class MockingTest {
	private Date fechaInicio;
	private Date fechaFin;

	@Rule
	public EasyMockRule mocks = new EasyMockRule(this);

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		fechaInicio = new Date(2015, 0, 1);
		fechaFin = new Date(2015, 3, 1);
	}
	
	@Test
	public void testObtenerPruebasNoCompletamenteCalificadasAsignatura() {
		Prueba prueba1 = createNiceMock(Prueba.class);
		Prueba prueba2 = createNiceMock(Prueba.class);
		Prueba prueba3 = createNiceMock(Prueba.class);
		Prueba prueba4 = createNiceMock(Prueba.class);
		Asignatura asignatura = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignatura.incluirPrueba(prueba1);
		asignatura.incluirPrueba(prueba2);
		asignatura.incluirPrueba(prueba3);		

		expect(prueba1.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba2.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba3.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba4.isCompletamenteCalificada()).andReturn(false).anyTimes();

		replay(prueba1);
		replay(prueba2);
		replay(prueba3);
		replay(prueba4);

		assertEquals(3, asignatura.numeroPruebas());
		
		verify(prueba1);
		verify(prueba2);
		verify(prueba3);
		verify(prueba4);
	}
	
	@Test
	public void testObtenerNotaPruebas() {
		Prueba prueba1 = createNiceMock(Prueba.class);
		Hashtable<String, Double> calificaciones = new Hashtable<String, Double>();
		Asignatura asignatura = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignatura.incluirPrueba(prueba1);	

		expect(prueba1.getCalificaciones()).andReturn(calificaciones).anyTimes();
		
		replay(prueba1);

		assertEquals(1, asignatura.numeroPruebas());
		
		verify(prueba1);
	}

}
