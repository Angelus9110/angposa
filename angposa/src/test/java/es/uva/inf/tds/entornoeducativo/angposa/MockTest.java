package es.uva.inf.tds.entornoeducativo.angposa;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MockingTest.class })

public class MockTest {

}
