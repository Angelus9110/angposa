package es.uva.inf.tds.entornoeducativo.angposa;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Hashtable;

import org.junit.Before;
import org.junit.Test;

public class TestAsignaturaTDD2 {
	private Date fechaInicio;
	private Date fechaFin;
	private Asignatura asignaturaOriginal;
	private Date fechaRealizacion;
	private Date fechaRealizacion2;
	private Date fechaRealizacion3;
	private Date fechaRealizacion4;
	private Date fechaRealizacion5;
	private Date fechaRealizacion6;
	private Date fechaCalificacion;
	private Prueba pruebaCorrecta;
	private Prueba pruebaFechaFueraIntervaloArriba;
	private Prueba pruebaFechaFueraIntervaloAbajo;
	private Date fechaRealizacionFueraAbajo;
	private Date fechaRealizacionFueraArriba;
	private Asignatura asignaturaFechaErronea;
	private Prueba pruebaPeso1;
	private Prueba pruebaPeso2;
	private Prueba prueba1CompletamenteCalificada;
	private Prueba prueba2CompletamenteCalificada;
	private Prueba prueba3CompletamenteCalificada;
	private Prueba prueba4CompletamenteCalificada;
	private Prueba prueba5CompletamenteCalificada;
	private Prueba prueba6CompletamenteCalificada;
	private Prueba prueba7NoCompletamenteCalificada;

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		fechaInicio = new Date(2015, 0, 1);
		fechaFin = new Date(2015, 3, 1);
		asignaturaOriginal = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignaturaFechaErronea = new Asignatura("nombre", "descripcion", 10, fechaFin, fechaInicio);
		fechaRealizacion = new Date(2015, 2, 1);
		fechaRealizacion2 = new Date(2015, 2, 10);
		fechaRealizacion3 = new Date(2015, 2, 15);
		fechaRealizacion4 = new Date(2015, 2, 18);
		fechaRealizacion5 = new Date(2015, 2, 25);
		fechaRealizacion6 = new Date(2015, 2, 27);
		fechaCalificacion = new Date(2017, 2, 28);
		fechaRealizacionFueraAbajo = new Date(2016, 11, 30);
		fechaRealizacionFueraArriba = new Date(2017, 5, 10);
		pruebaCorrecta = new Prueba(fechaRealizacion, "nombre", "descripcion", 3, 0.4);
		pruebaFechaFueraIntervaloArriba = new Prueba(fechaRealizacionFueraArriba, "nombre", "descripcion", 3, 0.4);
		pruebaFechaFueraIntervaloAbajo = new Prueba(fechaRealizacionFueraAbajo, "nombre", "descripcion", 3, 0.4);
		pruebaPeso1 = new Prueba(fechaRealizacion5, "nombre", "descripcion", 10, 0.9);
		pruebaPeso2 = new Prueba(fechaRealizacion2, "nombre", "descripcion", 7, 0.8);
		prueba1CompletamenteCalificada = new Prueba(fechaRealizacion, "nombre", "descripcion", 3, 0.4);
		prueba1CompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
		prueba1CompletamenteCalificada.marcarCalificada(fechaCalificacion);
		prueba2CompletamenteCalificada = new Prueba(fechaRealizacion2, "nombre", "descripcion", 2, 0.1);
		prueba2CompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
		prueba2CompletamenteCalificada.marcarCalificada(fechaCalificacion);
		prueba3CompletamenteCalificada = new Prueba(fechaRealizacion3, "nombre", "descripcion", 5, 0.1);
		prueba3CompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
		prueba3CompletamenteCalificada.marcarCalificada(fechaCalificacion);
		prueba4CompletamenteCalificada = new Prueba(fechaRealizacion4, "nombre", "descripcion", 10, 0.1);
		prueba4CompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
		prueba4CompletamenteCalificada.marcarCalificada(fechaCalificacion);
		prueba5CompletamenteCalificada = new Prueba(fechaRealizacion5, "nombre", "descripcion", 6, 0.2);
		prueba5CompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
		prueba5CompletamenteCalificada.marcarCalificada(fechaCalificacion);
		prueba6CompletamenteCalificada = new Prueba(fechaRealizacion6, "nombre", "descripcion", 6, 0.3);
		prueba6CompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
		prueba6CompletamenteCalificada.marcarCalificada(fechaCalificacion);
		prueba7NoCompletamenteCalificada = new Prueba(fechaRealizacion, "nombre", "descripcion", 3, 0.2);
		prueba7NoCompletamenteCalificada.calificar("asd1", 2.0, fechaCalificacion);
	}

	@Test
	public void testFechasCorrectasAsignatura() {
		assertEquals(1, asignaturaOriginal.compruebaFecha());
		assertEquals(-1, asignaturaFechaErronea.compruebaFecha());
	}

	@Test
	public void testFechaPruebaEnIntervaloAsignatura() {
		assertEquals(1, asignaturaOriginal.fechaPruebaValida(pruebaCorrecta));
		assertEquals(-1, asignaturaOriginal.fechaPruebaValida(pruebaFechaFueraIntervaloArriba));
		assertEquals(-1, asignaturaOriginal.fechaPruebaValida(pruebaFechaFueraIntervaloAbajo));
	}

	@Test
	public void testIncluirPruebaEnAsignatura() {
		Asignatura asignaturaConPrueba = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignaturaConPrueba.incluirPrueba(pruebaCorrecta);
		assertTrue(asignaturaConPrueba.contienePrueba(pruebaCorrecta));
	}

	@Test
	public void testNoIncluirPruebaEnAsignatura() {
		Asignatura asignaturaSinPrueba = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		assertFalse(asignaturaSinPrueba.contienePrueba(pruebaCorrecta));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIncluirPruebaSuperaMaximoPeso() {
		Asignatura asignaturaConPruebaPeso = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignaturaConPruebaPeso.comprobarPruebaValida(pruebaCorrecta);
		asignaturaConPruebaPeso.comprobarPruebaValida(pruebaPeso1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIncluirPruebaSuperaMaximaNota() {
		Asignatura asignaturaConPruebaNota = new Asignatura("nombre", "descripcion", 3.0, fechaInicio, fechaFin);
		asignaturaConPruebaNota.comprobarPruebaValida(pruebaCorrecta);
		asignaturaConPruebaNota.comprobarPruebaValida(pruebaPeso2);
	}

	@Test
	public void testNoIncluirPruebaRepetidaEnAsignatura() {
		Asignatura asignaturaConPrueba = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignaturaConPrueba.incluirPrueba(pruebaCorrecta);
		asignaturaConPrueba.incluirPrueba(pruebaCorrecta);
		assertEquals(1, asignaturaConPrueba.numeroPruebas());
	}

	@Test
	public void testPruebasCompletamenteCalificadas() {
		Asignatura asignaturaConPrueba = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignaturaConPrueba.incluirPrueba(prueba6CompletamenteCalificada);
		assertTrue(asignaturaConPrueba.pruebasCompletamenteCalificadas());
	}

	@Test
	public void testPruebasNoCompletamenteCalificadas() {
		Asignatura asignaturaConPrueba = new Asignatura("nombre", "descripcion", 10.0, fechaInicio, fechaFin);
		asignaturaConPrueba.incluirPrueba(prueba7NoCompletamenteCalificada);
		assertFalse(asignaturaConPrueba.pruebasCompletamenteCalificadas());
	}

	@Test(expected = IllegalStateException.class)
	public void testNotasListadoParcialNoCompletamenteCalificadas() {
		Asignatura asignaturaConPeso = new Asignatura("nombre", "descripcion", 10, fechaInicio, fechaFin);
		asignaturaConPeso.incluirPrueba(prueba1CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba2CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba3CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba4CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba7NoCompletamenteCalificada);
		@SuppressWarnings("unused")
		Hashtable<String, Double> calificaciones = new Hashtable<String, Double>();
		calificaciones = asignaturaConPeso.calificacionesParciales();
	}

	@Test(expected = IllegalStateException.class)
	public void testNotasListadoFinalNoCompletamenteCalificadas() {
		Asignatura asignaturaConPeso = new Asignatura("nombre", "descripcion", 10, fechaInicio, fechaFin);
		asignaturaConPeso.incluirPrueba(prueba1CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba2CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba3CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba4CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba7NoCompletamenteCalificada);
		@SuppressWarnings("unused")
		Hashtable<String, Double> calificaciones = new Hashtable<String, Double>();
		calificaciones = asignaturaConPeso.calificacionesFinales();
	}

	@Test(expected = IllegalStateException.class)
	public void testNotasListadoFinalPesoMenor() {
		Asignatura asignaturaConPeso = new Asignatura("nombre", "descripcion", 10, fechaInicio, fechaFin);
		asignaturaConPeso.incluirPrueba(prueba1CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba2CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba3CompletamenteCalificada);
		asignaturaConPeso.incluirPrueba(prueba4CompletamenteCalificada);
		@SuppressWarnings("unused")
		Hashtable<String, Double> calificaciones = new Hashtable<String, Double>();
		calificaciones = asignaturaConPeso.calificacionesFinales();
	}

}
