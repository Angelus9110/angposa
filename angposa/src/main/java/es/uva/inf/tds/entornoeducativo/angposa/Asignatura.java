package es.uva.inf.tds.entornoeducativo.angposa;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Esta clase implemente las asignaturas siguiendo la desiderata proporcionada
 * en el enunciado de esta practica
 * 
 * @author Angel Posada Garcia
 *
 */
public class Asignatura {
	private String nombre;
	private String descripcion;
	private double notaMax;
	private Date fechaInicio;
	private Date fechaFin;
	private ArrayList<Prueba> pruebas;

	/**
	 * Crea una nueva instancia de asignatura con los parametros que se
	 * especifican por parametro
	 * 
	 * @param nombre
	 *            String que representa el nombre de la asignatura
	 * @param descripcion
	 *            String que representa la descripcion de la asignatura
	 * @param notaMaxima
	 *            Double que representa al nota maxima a obtener en la
	 *            asignatura
	 * @param fechaInicio
	 *            Date que representa la fecha de inicio de la asignatura
	 * @param fechaFin
	 *            Date que representa la fecha de fin de la asignatura
	 * @throws IllegalArgumentException
	 *             cuando el valor de la nota maxima es mayor que 10 o menor que
	 *             1
	 */
	public Asignatura(String nombre, String descripcion, double notaMax, Date fechaInicio, Date fechaFin) {
		if (nombre == null || descripcion == null || fechaInicio == null || fechaFin == null) {
			throw new NullPointerException("Algun atributo para la asignatura no tiene valor");
		}
		if (notaMax > 10 || notaMax < 1) {
			throw new IllegalArgumentException("Nota maxima de la asignatura fuera del intervalo 1 - 10");
		}
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.notaMax = notaMax;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		pruebas = new ArrayList<Prueba>();
	}

	/**
	 * Devuelve la nota maxima de la asignatura de la instancia
	 * 
	 * @return Double que representa la nota maxima de la asignatura
	 */
	public double getNotaMaxima() {
		return notaMax;
	}

	/**
	 * Devuelve el nombre de la asignatura de la instancia
	 * 
	 * @return String que representa el nombre de la asignatura
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve la descripcion de l aasignatura de la instancia
	 * 
	 * @return String que representa la descripcion de la asignatura
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Devuelve al fecha de inicio de la asignatura de la instancia
	 * 
	 * @return Date que representa al fecha de inicio de la asignatura
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * Devuelve la fecha de fin de la asignatura de la instancia
	 * 
	 * @return Date que representa la fecha de fin de la asignatura
	 */
	public Date getFechaFin() {
		return fechaFin;
	}

	/**
	 * Comprueba si la fecha de inicio de la asignatura es anterior a la fecha
	 * de fin de la misma
	 * 
	 * @return {@code 1} si la fecha de inicio de la instancia es anterior a la
	 *         fecha de fin de la instancia, o {@code -1} si la fecha de inicio
	 *         de la instancia es no anterior a la fecha de fin de la instancia
	 */
	public int compruebaFecha() {
		if (fechaInicio.before(fechaFin)) {
			return 1;
		} else {
			return -1;
		}
	}

	protected boolean contienePrueba(Prueba prueba) {
		if (pruebas.contains(prueba)) {
			return true;
		} else {
			return false;
		}
	}

	protected int fechaPruebaValida(Prueba prueba) {
		if (prueba.getFecha().after(fechaInicio) && prueba.getFecha().before(fechaFin)) {
			return 1;
		}
		if (prueba.getFecha().compareTo(fechaInicio) == 0) {
			return 1;
		}
		if (prueba.getFecha().compareTo(fechaFin) == 0) {
			return 1;
		}
		return -1;
	}

	/**
	 * Devuelve el peso total de las pruebas para la asignatura de la instancia
	 * 
	 * @return double del porcentaje como un valor entre 0 y 1
	 */
	public double pesoPruebas() {
		double peso = 0;
		for (Prueba prueba : pruebas) {
			peso = peso + prueba.getPeso();
		}
		return peso;
	}

	/**
	 * Comprueba si todas las pruebas para la asignatura de la instancia han
	 * sido marcadas como calificadas
	 * 
	 * @return {@code true} si todas las pruebas de la asignatura han sido
	 *         marcadas como calificadas {@code false} en caso contrario
	 */
	public boolean pruebasCompletamenteCalificadas() {
		int pruebaCalificada = 0;
		for (Prueba prueba : pruebas) {
			if (prueba.isCompletamenteCalificada()) {
				pruebaCalificada = pruebaCalificada + 1;
			}
		}
		if (pruebas.size() == pruebaCalificada) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Añade una nueva prueba a la instancia correspondiente de asignatura, si
	 * la prueba ya esta en esa instancia de asignatura no la añadira
	 * 
	 * @param prueba
	 *            prueba a incluir en la instacia de asignatura
	 * 
	 * @throws IllegalArgumentException
	 *             Cuando la nueva prueba supera con su peso el peso maximo de
	 *             las pruebas
	 * 
	 * @throws IllegalArgumentException
	 *             Cuando la nueva prueba supera con su nota ponderada la nota
	 *             maxima de la asignatura
	 */
	public void incluirPrueba(Prueba prueba) {

		if (!contienePrueba(prueba)) {
			pruebas.add(prueba);
		}
	}

	/**
	 * Devuelve el numero de pruebas de la asignatura de la instancia
	 * 
	 * @return integer con el numero de pruebas de la asignatura
	 */
	public int numeroPruebas() {
		return pruebas.size();
	}

	/**
	 * Devuelve las calificaciones finales para la asignatura de la instancia
	 * 
	 * @return hashtable con los pares alumno nota final ponderada de la
	 *         asignatura
	 * 
	 * @throws IllegalStateException
	 *             si todas las pruebas de la asignatura no estan completamente
	 *             calificadas
	 * 
	 * @throws IllegalStateException
	 *             si el peso total de todas las pruebas no es igual a 1.0
	 */
	public Hashtable<String, Double> calificacionesFinales() {
		if (!pruebasCompletamenteCalificadas()) {
			throw new IllegalStateException("Las pruebas no estan completamente calificadas");
		}
		if (pesoPruebas() != 1.0) {
			throw new IllegalStateException("El peso total de las pruebas no es igual a 1.0");
		}
		Hashtable<String, Double> calificaciones = new Hashtable<String, Double>();
		Hashtable<String, Double> calificacionesFinales = new Hashtable<String, Double>();
		for (Prueba prueba : pruebas) {
			calificaciones = prueba.getCalificaciones();
			Enumeration<String> alumnos = calificaciones.keys();
			while (alumnos.hasMoreElements()) {
				if (calificacionesFinales.containsKey(alumnos.nextElement())) {
					calificacionesFinales.put(alumnos.nextElement(), (calificacionesFinales.get(alumnos.nextElement())
							+ (calificaciones.get(alumnos.nextElement()) * prueba.getPeso())));
				} else {
					if (alumnos.hasMoreElements()) {
						calificacionesFinales.put(alumnos.nextElement(),
								((calificaciones.get(alumnos.nextElement()) * prueba.getPeso())));
					}
				}
			}
		}
		return calificacionesFinales;
	}

	/**
	 * Devuelve las calificaciones finales para la asignatura de la instancia
	 * 
	 * @return hashtable con los pares alumno nota parcial ponderada de la
	 *         asignatura
	 * 
	 * @throws IllegalStateException
	 *             si todas las pruebas hasta el momento de la asignatura no
	 *             estan completamente calificadas
	 * 
	 */
	public Hashtable<String, Double> calificacionesParciales() {
		if (!pruebasCompletamenteCalificadas()) {
			throw new IllegalStateException(
					"Las pruebas realizadas hasta el momento no estan completamente calificadas");
		}
		Hashtable<String, Double> calificaciones = new Hashtable<String, Double>();
		Hashtable<String, Double> calificacionesParciales = new Hashtable<String, Double>();
		for (Prueba prueba : pruebas) {
			calificaciones = prueba.getCalificaciones();
			Enumeration<String> alumnos = calificaciones.keys();
			while (alumnos.hasMoreElements()) {
				if (calificacionesParciales.containsKey(alumnos.nextElement())) {
					calificacionesParciales.put(alumnos.nextElement(),
							(calificacionesParciales.get(alumnos.nextElement())
									+ (calificaciones.get(alumnos.nextElement()) * prueba.getPeso())));
				} else {
					if (alumnos.hasMoreElements()) {
						calificacionesParciales.put(alumnos.nextElement(),
								((calificaciones.get(alumnos.nextElement()) * prueba.getPeso())));
					}
				}
			}
		}
		return calificacionesParciales;
	}

	/**
	 * Comprueba si una nueva prueba cumple las condicyiones para incluirla en
	 * las pruebas de la instancia
	 * 
	 * @param prueba
	 *            prueba a comprobar en la instacia de asignatura
	 * 
	 * @throws IllegalArgumentException
	 *             Cuando la nueva prueba supera con su peso el peso maximo de
	 *             las pruebas
	 * 
	 * @throws IllegalArgumentException
	 *             Cuando la nueva prueba supera con su nota ponderada la nota
	 *             maxima de la asignatura
	 */
	public void comprobarPruebaValida(Prueba prueba) {
		double peso = 0;
		for (Prueba pruebaPeso : pruebas) {
			peso = peso + pruebaPeso.getPeso();
		}
		if ((peso + prueba.getPeso()) > 1) {
			throw new IllegalArgumentException("La nueva prueba supera el total de pesos junto con las otras pruebas");
		}
		double nota = 0;
		for (Prueba pruebaNota : pruebas) {
			nota = nota + (pruebaNota.getNotaMax() * pruebaNota.getPeso());
		}
		if ((nota + (prueba.getNotaMax() * prueba.getPeso())) > notaMax) {
			throw new IllegalArgumentException(
					"La nueva prueba supera el total de la nota maxima posible para esta asignatura");
		}
		incluirPrueba(prueba);
	}

}
