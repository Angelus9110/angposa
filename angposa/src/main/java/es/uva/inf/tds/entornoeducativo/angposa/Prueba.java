package es.uva.inf.tds.entornoeducativo.angposa;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Prueba de una asignatura. Puede calificarse. Tiene tanto un nombre, una breve
 * descripción, como una fecha de realización.
 * 
 * @author anonymous
 */
public class Prueba {
	protected Hashtable<String, Double> calificaciones;
	private Date fecha;
	private String nombre;
	private String descripcion;
	private double notaMax;
	private double peso;
	private boolean completamenteCalificada;

	/**
	 * Inicializa una prueba.
	 * 
	 * @param fecha
	 *            Fecha de realización de la prueba
	 * @param nombre
	 *            Identificador de la prueba
	 * @param descripcion
	 *            Breve información acerca de la prueba
	 * @param notaMax
	 *            nota máxima que puede obtenerse en la prueba
	 * @param peso
	 *            Porcentaje (valor entre 0 y 1) que la prueba tiene en una
	 *            asignatura
	 */
	public Prueba(Date fecha, String nombre, String descripcion, double notaMax, double peso) {
		this.fecha = fecha;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.notaMax = notaMax;
		this.peso = peso;
		completamenteCalificada = false;
		calificaciones = new Hashtable<String, Double>();
	}

	/**
	 * Informa si la prueba ha sido marcada como completamente calificada o no.
	 * 
	 * @return true si está completamente calificada, false en otro caso
	 */
	public boolean isCompletamenteCalificada() {
		return completamenteCalificada;
	}

	/**
	 * Devuelve todas las calificaciones de la prueba.
	 * 
	 * @return tabla hash con pares <identificador de alumno,nota>
	 */
	public Hashtable<String, Double> getCalificaciones() {
		return calificaciones;
	}

	/**
	 * Marca una prueba como que ha sido completamente calificada. Esto no puede
	 * revertirse.
	 * 
	 * @param fechaActual
	 * @throws IllegalStateException
	 *             si la fecha en la que se marca como calificada es anterior a
	 *             la fecha de realización de la prueba
	 * @throws IllegalStateException
	 *             si no hay ninguna calificación
	 */
	public void marcarCalificada(Date fechaActual) {
		if (fechaActual.before(fecha)) {
			throw new IllegalStateException("La fecha de calificacion es anterior a al fecha de realizacion");
		}
		if (calificaciones.isEmpty()) {
			throw new IllegalStateException("No hay calificaciones para todos los alumnos");
		}
		completamenteCalificada = true;
	}

	/**
	 * Añade una calificación a una prueba. La nota no podrá ser mayor que la
	 * nota máxima y debe añadirse después de la celebración de la prueba. No
	 * debe existir ya una calificación para dicho alumno.
	 * 
	 * @param id
	 *            es el identificador del alumno.
	 * @param nota
	 *            es la nota obtenida en la prueba.
	 * @param fechaActual
	 *            es la fecha en que se añade la calificación
	 * @throws IllegalStateException
	 *             si la fecha en la que se califica es anterior a la
	 *             celebración de la prueba
	 * @throws IllegalStateException
	 *             si la prueba ya está completamente calificada
	 * @throws IllegalArgumentException
	 *             si el alumno ya tiene una calificación en la prueba
	 * @throws IllegalArgumentException
	 *             si la nota supera la nota máxima de la prueba
	 */
	public void calificar(String id, double nota, Date fechaActual) {
		if (fechaActual.before(fecha)) {
			throw new IllegalStateException("La fecha de calificacion es anterior a al fecha de realizacion");
		} else if (calificaciones.get(id) != null) {
			throw new IllegalArgumentException("Ya hay una calificacion para el alumno");
		} else if (completamenteCalificada) {
			throw new IllegalStateException("La prueba ya esta completamente calificada");
		} else if (nota > notaMax) {
			throw new IllegalArgumentException("La nota de la prueba es superior a al nota maxima de la misma");
		} else {
			calificaciones.put(id, nota);
		}
	}

	/**
	 * Añade una lista de calificaciones a una prueba. Las notas no podrán ser
	 * mayores que la nota máxima y debe añadirse después de la celebración de
	 * la prueba. No deben existir ya notas para ninguno de sus alumnos.
	 * 
	 * @param calificaciones
	 *            tabla hash con pares identificador de alumno, nota
	 * @param fechaActual
	 *            es la fecha en la que se califica
	 * @throws IllegalStateException
	 *             si la fecha en la que se califica es anterior a la
	 *             celebración de la prueba
	 * @throws IllegalStateException
	 *             si la prueba ya está completamente calificada
	 * @throws IllegalArgumentException
	 *             si algún alumno ya tiene una calificación en la prueba
	 * @throws IllegalArgumentException
	 *             si alguna nota supera la nota máxima de la prueba
	 */
	public void calificar(Hashtable<String, Double> calificaciones, Date fechaActual) {
		if (fechaActual.before(fecha)) {
			throw new IllegalStateException("La fecha de calificacion es anterior a al fecha de realizacion");
		} else if (calificaciones != null) {
			throw new IllegalArgumentException("Ya hay una calificacion para esta prueba");
		} else if (completamenteCalificada) {
			throw new IllegalStateException("La prueba ya esta completamente calificada");
		}
		@SuppressWarnings("null")
		Enumeration<Double> ListaNotas = calificaciones.elements();
		if (ListaNotas.hasMoreElements()) {
			if (notaMax < ListaNotas.nextElement()) {
				throw new IllegalArgumentException("Alguna de las notas es superior a la nota maxima de la prueba");
			}
		} else {
			this.calificaciones = calificaciones;
		}
	}

	/**
	 * Modifica la nota de una calificación ya existente.
	 * 
	 * @param id
	 *            identificador del alumno al que deseamos modificar la nota
	 * @param nota
	 *            nueva nota
	 * @throws IllegalArgumentException
	 *             si el alumno no tiene una nota aún
	 * @throws IllegalArgumentException
	 *             si la nota supera la nota máxima de la prueba
	 */
	public void modificar(String id, double nota) {
		if (!calificaciones.containsKey(id)) {
			throw new IllegalArgumentException("No hay una calificacion para el alumno");
		} else if (nota > notaMax) {
			throw new IllegalArgumentException("La nota de la prueba es superior a al nota maxima de la misma");
		} else {
			calificaciones.put(id, nota);
		}
	}

	/**
	 * Peso de la prueba en la asignatura
	 * 
	 * @return Porcentaje como un valor entre 0 y 1
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * Nombre de la prueba
	 * 
	 * @return retorna siempre una cadena no vacía
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Breve descripción de la prueba.
	 * 
	 * @return retorna siempre una cadena no vacía
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Nota máxima de la prueba.
	 * 
	 * @return retorna siempre un número mayor que 0
	 */
	public double getNotaMax() {
		return notaMax;
	}

	/**
	 * Fecha de celebración de la prueba
	 * 
	 * @return fecha conectada (no null).
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * Devuelve la nota de un alumno.
	 * 
	 * @param alumno
	 *            es el identificador del alumno
	 * @return nota es la nota del alumno en la prueba
	 * @throws IllegalArgumentException
	 *             si no existe ese alumno en el listado de calificaciones de la
	 *             prueba
	 */
	public double getNota(String alumno) {
		if (!calificaciones.containsKey(alumno)) {
			throw new IllegalArgumentException("No exixte ese alumno en el listado de calificaciones de la prueba");
		}
		return calificaciones.get(alumno);
	}
}
